import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AkashCounterComponent } from './akash-counter/akash-counter.component';
import { AdditinComponent } from './additin/additin.component';
import{HttpClientModule} from '@angular/common/http';
import { HomepageComponent } from './homepage/homepage.component';
import { MenuComponent } from './menu/menu.component';
import { Task1Component } from './task1/task1.component';
import { Task2Component } from './task2/task2.component';
import { Task3Component } from './task3/task3.component';
import { Task4Component } from './task4/task4.component';
import { Christmas2022Component } from './christmas2022/christmas2022.component';
import { Task5Component } from './task5/task5.component';
import { Task6Component } from './task6/task6.component';
import { Task7Component } from './task7/task7.component';
import { Task1problem1Component } from './task1problem1/task1problem1.component';
import { Task1problem2Component } from './task1problem2/task1problem2.component'


@NgModule({
  declarations: [
    AppComponent,
    AkashCounterComponent,
    AdditinComponent,
    HomepageComponent,
    MenuComponent,
    Task1Component,
    Task2Component,
    Task3Component,
    Task4Component,
    Christmas2022Component,
    Task5Component,
    Task6Component,
    Task7Component,
    Task1problem1Component,
    Task1problem2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
