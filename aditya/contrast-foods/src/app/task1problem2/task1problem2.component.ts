import { Component } from '@angular/core';

@Component({
  selector: 'app-task1problem2',
  templateUrl: './task1problem2.component.html',
  styleUrls: ['./task1problem2.component.css']
})
export class Task1problem2Component {

  click_problem=2;
  
  clicked_star=0;
}
