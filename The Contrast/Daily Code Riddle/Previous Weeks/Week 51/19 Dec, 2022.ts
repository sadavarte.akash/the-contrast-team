// @ts-nocheck

function guess_the_movie(clue){
    console.log('Guess the movie with cast', clue);
}

let casts = []
casts.push(['Akshay Kumar','Preity Zinta'])
casts.push(['Sunil Shetty','Shilpa Shetty','Akshay Kumar'])
casts.push(['Nana Patekar','Dimple Kapadia'])

for(let i=0;i<casts.length;i++){
    guess_the_movie(casts[i])
}