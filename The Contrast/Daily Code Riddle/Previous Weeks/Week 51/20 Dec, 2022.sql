Guess the Answer?

-- Riddle 1
-- How much do you know your friends?

Select first_name, instagram_id
    From Friends_and_Relatives
    Where Month(birth_date) == '12'


-- Riddle 2
-- How much do you know your friends?

Select first_name, instagram_id
    From Friends_and_Relatives
    Where "Favourite Color" = "Red"
        OR "Favourite Color" = "White"