// @ts-nocheck

function guess_the_movie(clue){
    console.log('Guess the movie with cast', clue);
}

let casts = [
    ['Paresh Rawal','Raveena Tandon'],
    ['Allu Arjun','Rashmika'],
    ['Salman Khan','Shahrukh Khan','Madhuri Dixit']
]

casts.forEach(cast=>{
    guess_the_movie(cast)
})