import { Component } from '@angular/core';

@Component({
  selector: 'app-task3',
  templateUrl: './task3.component.html',
  styleUrls: ['./task3.component.css']
})
export class Task3Component {

  fibonacci:number[]=[];
// str:any[]=[];
input_value=0;

fibonacci_series() {
  this.fibonacci=[1,1]
  for (let i=2; i<10; i++ ){
    this.fibonacci[i]= this.fibonacci[i-2] + this.fibonacci[i-1];
  }
  return  this.fibonacci;

}


// print_stars(){
//   this.str = ['*','*']
//   for (let i=2; i<50; i++ ){
//         this.str[i]= this.str[i-2] + this.str[i-1];
//       }

//   return this.str;

// }


}
