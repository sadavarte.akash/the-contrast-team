import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { MenuComponent } from './menu/menu.component';
import { Task1Component } from './task1/task1.component';
import { Task2Component } from './task2/task2.component';
import { Task3Component } from './task3/task3.component';
import { Task4Component } from './task4/task4.component';
import { Task5Component } from './task5/task5.component';
import { Task6Component } from './task6/task6.component';



const routes: Routes = [
  {path:'home', component:HomepageComponent},
  {path:'menu', component: MenuComponent},
  {path:'Task1',component:Task1Component},
  {path:'Task2',component:Task2Component},                                                                                                                                                       
  {path:'Task3',component:Task3Component},                                                                                                                                                       
  {path:'Task4',component:Task4Component},                                                                                                                                                       
  {path:'Task5',component:Task5Component},                                                                                                                                                       
  {path:'Task6',component:Task6Component},                                                                                                                                                       
  {path : '', redirectTo:'home', pathMatch:'full'  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
