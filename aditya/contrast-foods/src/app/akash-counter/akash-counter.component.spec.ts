import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AkashCounterComponent } from './akash-counter.component';

describe('AkashCounterComponent', () => {
  let component: AkashCounterComponent;
  let fixture: ComponentFixture<AkashCounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AkashCounterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AkashCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
