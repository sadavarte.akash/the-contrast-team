import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task1',
  templateUrl: './task1.component.html',
  styleUrls: ['./task1.component.css']
})
export class Task1Component {

  constructor(public ar:ActivatedRoute){


    
    this.ar.queryParams.subscribe((data:any)=>{
      console.log(data);
      this.click_problem=data.problem

    })

  }
  click_problem=0;
  
  


}
