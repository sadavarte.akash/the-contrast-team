import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Lecture1Component } from './lectures/lecture1/lecture1.component';
import { Lecture2Component } from './lectures/lecture2/lecture2.component';
import { Lecture3Component } from './lectures/lecture3/lecture3.component';
import { Lecture1problem1Component } from './lectures/lecture1/problem/lecture1problem1/lecture1problem1.component';
import { Lecture1problem2Component } from './lectures/lecture1/problem/lecture1problem2/lecture1problem2.component';

@NgModule({
  declarations: [
    AppComponent,
    Lecture1Component,
    Lecture2Component,
    Lecture3Component,
    Lecture1problem1Component,
    Lecture1problem2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
