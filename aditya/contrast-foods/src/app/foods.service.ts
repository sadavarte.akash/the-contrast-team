import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FoodsService {

  constructor() { }
  list =[
    {
      image:'https://i.pinimg.com/736x/03/35/14/03351403ae27d274e94f1383358f003a.jpg',
      discount:'50% OFF up to 100',
      time:'30 min',
      star:'4.0 ',
      menu:'Pizza,Sandwich,Fast Food',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'3752+ orders place from here recently',
      
    },
    {
      image:'https://media.istockphoto.com/id/904617420/photo/fresh-mango-smoothie-in-the-glass.jpg?s=612x612&w=0&k=20&c=ogIRn5AfahJNU4W8UmQIZ-mJqL9tgOm9yH_-5WJmkSQ=',
      discount:'50% OFF up to 100',
      time:'34 min',
      star:'3.4 ',
      menu:'Healthy Food,Sandwich',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'1972+ orders place from here recently',
    
    },
    {
      image:'https://www.voguehk.com/media/2019/01/208_KW_Burrata_Pizza_Artigiano.jpg',
      discount:'50% OFF up to 100',
      time:'28 min',
      star:'3.0 ',
      menu:'Pizza,Italian,Desserts',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'889+ orders place from here recently ',
     
    },
    {
      image:'https://slicelife.imgix.net/54258/photos/original/sbarro1_720.jpg?auto=compress&auto=format',
      discount:'50% OFF up to 100',
      time:'35 min',
      star:'New ',
      menu:'Pizza,Burger,Italian',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'350+ orders place from here recently',
      
    },
    {
      image:'https://thumbs.dreamstime.com/b/sandwiches-mozzarella-basil-tomatoes-italian-sandwich-grilled-italian-sausage-sandwich-mozzarella-tomatoes-159248191.jpg',
      discount:'50% OFF up to 100',
      time:'21 min',
      star:'4.1 ',
      menu:'Sandwich,Panini,Pizza',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'3225+ orders place from here recently',
      
    },
    {
      image:'https://thumbs.dreamstime.com/b/assorted-american-food-assorted-american-food-wood-background-109748386.jpg',
      discount:'40% OFF',
      time:'30 min',
      star:'4.2 ',
      menu:'Coffe,Burger,Pizza',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'Restaurant partner follows WHO protocol',
    
    },
    {
      image:'https://b.zmtcdn.com/data/pictures/4/5594/87893ace3b48485d9d69e9c5853843d5_featured_v2.jpg',
      discount:'50% OFF up to 100',
      time:'30 min',
      star:'3.6 ',
      menu:'Fast Food,Coffee',
      price:'250 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'3752+ orders place from here recently',
     
    },
    {
      image:'https://restaurantclicks.com/wp-content/uploads/2022/10/McDonalds-Copycat-Recipes.jpg',
      discount:'50% OFF up to 100',
      time:'29 min',
      star:'3.4 ',
      menu:'Burger,Bevrages',
      price:'250 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'19072+ orders place from here recently ',
      
    },
    {
      image:'https://live.staticflickr.com/359/31619504032_1968522fcf_b.jpg',
      discount:'50% OFF up to 100',
      time:'24 min',
      star:'4.9 ',
      menu:'Chai,Cream Roll,Samosa',
      price:'150 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'3889+ orders place from here recently',
      
    },
    {
      image:'https://b.zmtcdn.com/data/pictures/1/19564671/46eb6b46da4f59c8afaa83240b70b8dd_featured_v2.jpg',
      discount:'50% OFF up to 100',
      time:'52 min',
      star:'4.1 ',
      menu:'Caffe,Healthy Food',
      price:'400 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'Restaurant partner follows WHO protocol',
      
    },
    {
      image:'https://media.istockphoto.com/id/154962256/photo/pita-pocket-with-grilled-chicken.jpg?s=612x612&w=0&k=20&c=kEG_Mn60SM5uHuiaAWePEZWcily0MbXN0wJYhzFoeOg=',
      discount:'50% OFF up to 100',
      time:'31 min',
      star:'4.9 ',
      menu:'Sandwich,Coffe,Dessert',
      price:'250 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'4079+ orders place from here recently ',
      
    },
    {
      image:'https://res.cloudinary.com/swiggy/image/upload/f_auto,q_auto,fl_lossy/lykhrcat5nv4pt3vimsu',
      discount:'50% OFF up to 100',
      time:'29 min',
      star:'3.7 ',
      menu:'Waffel,Burger,Dessert',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'6424+ orders place from here recently',
     
    },
    {
      image:'http://www.sanjeevkapoor.com/UploadFiles/RecipeImages/Vegetable-Manchow-Soup-Sanjeev-Kapoor-Kitchen-FoodFood.JPG',
      discount:'50% OFF up to 100',
      time:'40 min',
      star:'4.5  ',
      menu:'North Indian,Kebab,Chiken',
      price:'300 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'1978+ orders place from here recently',
     
    },
    {
      image:'https://cdn.shopify.com/s/files/1/0549/6035/7436/products/chicken-lollipop_result.webp?v=1663044499',
      discount:'50% OFF up to 100',
      time:'30 min',
      star:'4.0 ',
      menu:'Thai,Chines,Masharom',
      price:'150 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'Follows all Max Safety measures to ensure your food is safe',
    
    },
    {
      image:'https://media.istockphoto.com/id/1189079893/photo/aloo-paratha-with-lassi-indian-potato-stuffed-flatbread-with-butter-on-top-served-with-fresh.jpg?s=612x612&w=0&k=20&c=KS-HPML7jXEVCD4edsaaAj6uLVqZUI7sxdSf0CtQKsc=',
      discount:'50% OFF up to 100',
      time:'32 min',
      star:'3.9 ',
      menu:'North Indian,Street Food',
      price:'200 for one',
      delivery:'https://us.123rf.com/450wm/frender/frender2008/frender200800098/153840253-free-delivery-concept-3d-illustration-isolated-on-white-background.jpg?ver=6',
      oders:'5052+ orders place from here recently',
     
    },

     
    
  ]  
    
    
  } 
    
  
  
  

