import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Task1Component } from './task/task1/task1.component';




const routes: Routes = [
  {path:'Task1',component:Task1Component},
  {path:'Task2',component:Task2Component},
  {path:'Task3',component:Task3Component},
  {path:'Task4',component:Task4Component},
  {path:'Task5',component:Task5Component},



]
 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
