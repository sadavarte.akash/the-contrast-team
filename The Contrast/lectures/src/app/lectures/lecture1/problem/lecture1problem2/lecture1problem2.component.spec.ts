import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lecture1problem2Component } from './lecture1problem2.component';

describe('Lecture1problem2Component', () => {
  let component: Lecture1problem2Component;
  let fixture: ComponentFixture<Lecture1problem2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lecture1problem2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lecture1problem2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
