// @ts-nocheck


let relations = {
    'Akash Sadavarte': {
        'Dhanashree Shinde': 'Student',
        'Aditya Nehere': 'Student',
        'Seema Sadavarte': 'Mother',
    },
    'Dhanashree Shinde': {
        'Akash Sadavarte': 'Mentor and IT Trainer'
    }
    // ... And so on
}

function get_relation(person1, person2) {
    return relations[person1][person2]
}

function get_relative(person1, relation) {
    let relations = relations[person1]
    let people = Object.keys[relations]
    people.find(person => relations[person] == relation);
}

console.log(
    get_relation('You',
        get_relative('You',
            get_relation('You',
                get_relative('You', 'Best Friend')))))


