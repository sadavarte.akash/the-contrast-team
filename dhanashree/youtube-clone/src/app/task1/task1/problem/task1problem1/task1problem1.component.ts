import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task1problem1',
  templateUrl: './task1problem1.component.html',
  styleUrls: ['./task1problem1.component.css']
})
export class Task1problem1Component {
  
  color='red'
  change_color(){
    if (this.color=='red'){
      this.color='black';
    }else if (this.color=='black')
      this.color='red'
  }

}
