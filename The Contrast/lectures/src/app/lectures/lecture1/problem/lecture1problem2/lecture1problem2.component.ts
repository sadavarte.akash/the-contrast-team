import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lecture1problem2',
  templateUrl: './lecture1problem2.component.html',
  styleUrls: ['./lecture1problem2.component.css']
})
export class Lecture1problem2Component implements OnInit {

  clicked_star = 0;

  my_array:any[]=[]
  number_of_stars = 5;

  constructor() { }

  ngOnInit(): void {
    for(let i=0;i<this.number_of_stars;i++){
      this.my_array.push(i+1)
    }
  }

}
