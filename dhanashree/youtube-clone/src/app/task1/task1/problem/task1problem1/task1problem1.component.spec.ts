import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Task1problem1Component } from './task1problem1.component';

describe('Task1problem1Component', () => {
  let component: Task1problem1Component;
  let fixture: ComponentFixture<Task1problem1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Task1problem1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Task1problem1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
