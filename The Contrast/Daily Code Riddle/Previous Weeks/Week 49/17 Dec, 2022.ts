// @ts-nocheck

function guess_the_song(clue){
    console.log('Guess the song with clue', clue);
}

let song_clues = []
song_clues.push(['From the Color of the Flowers','Kishor Kumar'])
song_clues.push(['Atif Aslam','Without You','How should I live?'])
song_clues.push(['Sonu Nigam','Shreya Ghoshal','Vidya Balan','Sanjay Dutt'])

for(let i=0;i<song_clues.length;i++){
    guess_the_song(song_clues[i])
}