// @ts-nocheck


class Friend {
    constructor(public name,public balance) { }
    add_balance(balance){this.balance = this.balance + balance  }
    subtract_balance(balance){  this.balance = this.balance - balance }
}

let x = new Friend('Akash',6000);
let y = new Friend('Kunal',5000);
let z = new Friend('Chandan',1000);

let friends = [x,y,z]

console.log('Which of the friend from',friends,' has max balance?')

x.add_balance(2000); y.add_balance(1000); y.add_balance(5000);
z.subtract_balance(2000); y.subtract_balance(2000); x.subtract_balance(4000);

console.log('Which of the friend from',friends,' has max balance?')
console.log('Total Balance of Friends ',friends,'is?')



