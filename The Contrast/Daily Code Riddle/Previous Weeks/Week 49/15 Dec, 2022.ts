// @ts-nocheck

let tv_or_web_series = [
    'Friends', 'Mahabharat', 'Big Bang Theory', /* ... And So On */
]

let characters = {
    'Friends': ['Joey', 'Chandler', 'Monica', 'Ross', 'Rachael', 'Phoebe'],
    'Mahabharat': ['Krishna', 'Arjun', 'Yudhishtir']
}

let friends = ['Chandan', 'Sumeet', 'Sachin', /* ... Your Friends */]

let relatable = (friend, tv_show) => {
    let characters = characters[tv_show]
    return console.log('In the TV Show', tv_show,
        'Your Friend', friend,
        'Best relates to this character : ')
}

friends.forEach(friend => {
    relatable(friend,YOUR_FAVOURITE_SERIES)
})