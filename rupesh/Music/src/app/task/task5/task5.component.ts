import { Component } from '@angular/core';

@Component({
  selector: 'app-task5',
  templateUrl: './task5.component.html',
  styleUrls: ['./task5.component.css']
})
export class Task5Component {
  clicked_task=1
  color='red'
  change_color(){
    if (this.color=='red'){
      this.color='black';
    }else if (this.color=='black')
      this.color='red'
  }
  clicked_star=0
}
