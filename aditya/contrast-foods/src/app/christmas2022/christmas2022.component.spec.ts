import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Christmas2022Component } from './christmas2022.component';

describe('Christmas2022Component', () => {
  let component: Christmas2022Component;
  let fixture: ComponentFixture<Christmas2022Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Christmas2022Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Christmas2022Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
