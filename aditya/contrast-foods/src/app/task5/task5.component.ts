import { Component } from '@angular/core';
import { __values } from 'tslib';

@Component({
  selector: 'app-task5',
  templateUrl: './task5.component.html',
  styleUrls: ['./task5.component.css']
})
export class Task5Component {

   fibonacci:number[]=[];
  //  str:any[]=[];
   input_value = 0;
  
 
  listfibonacci() {
    this.fibonacci  = [1,1]
    for (let i= 2; i<50; i++) {
       this.fibonacci[i] =  this.fibonacci[i - 2]  +  this.fibonacci[i - 1] ;
    }
    return  this.fibonacci;
  }
 
  // print_stars() {
  //   this.str = ['*','*']
  //   for (let i= 1; i<100; i++) {
  //     this.str[i]=this.str [i-2]+ this.str[i-1]
  //   }
  //   return  this.str;
  // }


}
