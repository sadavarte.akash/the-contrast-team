import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-lecture1',
  templateUrl: './lecture1.component.html',
  styleUrls: ['./lecture1.component.css']
})
export class Lecture1Component implements OnInit {

  color = 'red'

  clicked_problem = 2

  clicked_star = 0;

  my_array:any[]=[]
  number_of_stars = 5;

  constructor(public ar:ActivatedRoute, public router:Router) {
    // let akash = (data:any)=>{
    //   console.log(data);
    //   this.clicked_problem = data.problem
    // }
    this.ar.queryParams.subscribe( (data:any)=>{
      console.log(data);
      this.clicked_problem = data.problem
    })
  }

  ngOnInit(): void {
    for(let i=0;i<this.number_of_stars;i++){
      this.my_array.push(i+1)
    }
  }

  change_color(){
    if(this.color=='red'){
      this.color='black'
    } else if (this.color=='black'){
      this.color = 'red'
    }

  }

  // change_problem(n:any){
  //   this.clicked_problem = n; 
  // }

}
