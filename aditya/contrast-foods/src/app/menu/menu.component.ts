import { Component } from '@angular/core';
import { FoodsService } from '../foods.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {

  constructor(public menu:FoodsService){
  }
}
