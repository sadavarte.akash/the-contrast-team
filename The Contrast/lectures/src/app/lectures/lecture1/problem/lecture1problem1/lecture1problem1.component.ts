import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lecture1problem1',
  templateUrl: './lecture1problem1.component.html',
  styleUrls: ['./lecture1problem1.component.css']
})
export class Lecture1problem1Component implements OnInit {
  color = 'red  '
  change_color(){
    if(this.color=='red'){
      this.color='black'
    } else if (this.color=='black'){
      this.color = 'red'
    }

  }

  constructor() { }

  ngOnInit(): void {
  }

}
