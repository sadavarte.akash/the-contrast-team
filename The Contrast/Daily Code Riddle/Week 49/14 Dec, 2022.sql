Guess the Answer?

-- Riddle 1
-- How much do you know Navi Mumbai?

Select City 
    From Navi Mumbai
        Where is_city_planned = true
            AND starts with 'K';


-- Riddle 2
-- Let's see if you know it

SELECT TOP 3 first_name, birth_date
     FROM Friends 
ORDER BY trust, honesty, freedom, affection;