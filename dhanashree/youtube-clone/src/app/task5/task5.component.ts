import { Component } from '@angular/core';

@Component({
  selector: 'app-task5',
  templateUrl: './task5.component.html',
  styleUrls: ['./task5.component.css']
})
export class Task5Component {

  input_value= 0;

  array:any[] =[1,2,3,4,5,6,7,8,9,10]

  add_to_array(){
    this.array = []
    for(let i=0; i<this.input_value;i++){
      this.array.push(i+1)
    }
  }

  // print_stars(m:number){
  //   let str = ''
  //   for(let i=0;i<m;i++){
  //     str = str + '*'
  //   }
  
  //   return str;
  
  // }

  
}
