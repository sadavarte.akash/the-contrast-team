import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Lecture1Component } from './lectures/lecture1/lecture1.component';
import { Lecture1problem1Component } from './lectures/lecture1/problem/lecture1problem1/lecture1problem1.component';
import { Lecture1problem2Component } from './lectures/lecture1/problem/lecture1problem2/lecture1problem2.component';
import { Lecture2Component } from './lectures/lecture2/lecture2.component';
import { Lecture3Component } from './lectures/lecture3/lecture3.component';

const routes: Routes = [
  {
    path:'lecture-1',
    component:Lecture1Component,
    // children : [
    //   { path:'problem-1', component : Lecture1problem1Component },
    //   { path:'problem-2', component : Lecture1problem2Component }
    // ]
  },
  {path:'lecture-2',component:Lecture2Component, children:[]},
  {path:'lecture-3',component:Lecture3Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
