import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
// import { YComponent } from './y/y.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { Task1Component } from './task1/task1.component';
import { Task2Component } from './task2/task2.component';
import { FoodBlogComponent } from './food-blog/food-blog.component';
import { Task3Component } from './task3/task3.component';
import { Task4Component } from './task4/task4.component';
import { Task5Component } from './task5/task5.component';
import { Task6Component } from './task6/task6.component';
import { Task1problem1Component } from './task1/task1/problem/task1problem1/task1problem1.component';
import { Task1problem2Component } from './task1/task1/problem/task1problem2/task1problem2.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { ChannelPageComponent } from './channel-page/channel-page.component';


@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    HomeComponent,
    Task1Component,
    Task2Component,
    FoodBlogComponent,
    Task3Component,
    Task4Component,
    Task5Component,
    Task6Component,
    Task1problem1Component,
    Task1problem2Component,
    PlaylistComponent,
    ChannelPageComponent
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
