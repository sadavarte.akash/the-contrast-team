import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskComponent } from './task/task/task.component';
import { Task1Component } from './task/task1/task1.component';
import { Task2Component } from './task/task2/task2.component';
import { Task3Component } from './task/task3/task3.component';
import { Task4Component } from './task/task4/task4.component';
import { Task5Component } from './task/task5/task5.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    Task1Component,
    Task2Component,
    Task3Component,
    Task4Component,
    Task5Component
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
