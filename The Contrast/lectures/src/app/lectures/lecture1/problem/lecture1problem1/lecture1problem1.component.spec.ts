import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lecture1problem1Component } from './lecture1problem1.component';

describe('Lecture1problem1Component', () => {
  let component: Lecture1problem1Component;
  let fixture: ComponentFixture<Lecture1problem1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lecture1problem1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lecture1problem1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
