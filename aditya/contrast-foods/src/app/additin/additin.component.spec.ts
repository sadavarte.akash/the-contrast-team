import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditinComponent } from './additin.component';

describe('AdditinComponent', () => {
  let component: AdditinComponent;
  let fixture: ComponentFixture<AdditinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdditinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
