// JavaScript Program To Print Hello World

const { read } = require('fs');

// console.log("Hello World");

// JavaScript Program to Add Two Numbers

// let a = 10 ;
// let b = 10 ;
// let c = a+b;
// console.log(c);

// const readline = require('readline').createInterface({
//       input: process.stdin,
//       output: process.stdout
//     });
// var a ;
// var b ;
// readline.question('enter first number ', function(Integer){
//     a = Integer;
//     readline.question('enter second number ', function(Integer){
//         b = Integer;
//         console.log("Your addition is ", parseInt(a) + parseInt(b));
//     readline.close();
// });
// });

// JavaScript Program to Find the Square Root

// const readline = require('readline').createInterface({
//       input: process.stdin,
//       output: process.stdout
//     });
// var a ;
// readline.question('enter number ', function(Integer){
//     a = Integer;
//     console.log("Square Root is ", parseInt(Math.sqrt(a)));
//     readline.close();
// })

// JavaScript Program to Calculate the Area of a Triangle

// const readline = require('readline').createInterface({
//       input: process.stdin,
//       output: process.stdout
//     });
// var a ;
// var b ;
// var c ;
// readline.question('enter base ', function(Integer){
//     a = Integer;
//     readline.question('enter height ', function(Integer){
//         b = Integer;
//         c = parseInt(a) * parseInt(b) / 2;
//         console.log("Area of a Triangle is ", c);
//         readline.close();
//     })
// })

// JavaScript Program to Swap Two Variables (with temp variable)

// const readline = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout
// });
// let a;
// let b;
// let temp;
// readline.question('Enter first variable : ', a => {
    
//     readline.question('Enter second variable : ', b => {
//         temp = a;
//         a = b;
//         b = temp;
//         console.log('First variable after swapping : ', a);
//         console.log('Second variable after swapping : ', b);
//         readline.close();
//     })
// })

// let a = "bhavesh";
// let b = "karkare";
// let temp = a;
// a = b;
// b = temp;
// console.log('First variable after swapping', a);
// console.log('Second variable after swapping', b);

// JavaScript Program to Swap Two Variables (with temp variable)

// let a = 1;
// let b = 2;
// a = a + b;
// b = a - b;
// a = a - b;
// console.log('First variable after swapping', a);
// console.log('Second variable after swapping', b);

// Javascript Program to Solve Quadratic Equation
// let root1, root2;
// let a=1, b=-3, c=10;
// let disciminant;
// disciminant = (b * b ) - 4 * a * c;
// if (disciminant>0){
//     root1 = (-b + Math.sqrt(disciminant)) / (2*a);
//     root2 = (-b - Math.sqrt(disciminant)) / (2*a);
//     console.log('The roots of Quadratic Equation are ', root1 + ' and ', root2);
// }else if(disciminant == 0){
//     root1 = root2 = (- b / (2 * a));
//     console.log('The roots of Quadratic Equation are ', root1 + ' and ', root2);
// }else {
//     let realPart = (-b / (2 * a)).toFixed(2);
//     let imagPart = (-Math.sqrt(disciminant) / (2 * a)).toFixed(2);
//     console.log('The roots of quadratic equation are ', realPart + '+', imagPart + ' and ', realPart + '-', imagPart);
// }

// JavaScript Program to Convert Kilometers to Miles
// const readline = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout
// })
// let k;
// let m=0.6213712;
// readline.question('Enter Kilometer value for converting into Miles : ', k => {
//     k = k * m;
//     console.log('Miles : ', k);
// readline.close();
// })

// Javascript Program to Convert Celsius to Fahrenheit
// const readline = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout
// })
// let c;
// let f;
// readline.question('Enter Celsius value for converting into Fahrenheit : ', c => {
//     f = (parseInt(c) * 1.8 ) + 32;
//     console.log('Fahrenheit : ',f);
// readline.close();
// })

// Javascript Program to Generate a Random Number
// let a = Math.floor(Math.random() * 6 +1);
// console.log('Random Number : ', a);

// Javascript Program to Check if a number is Positive, Negative, or Zero
// const readline = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout
// })
// let a;
// readline.question('Enter Number : ', a => {
//     if(a == 0){
//         console.log('Your number is Zero');
//     }else if(a> 0){
//         console.log('Your number is Positive');
//     }else{
//         console.log('Your number is Negative');
//     }
// readline.close();
// })

// Javascript Program to Check if a Number is Odd or Even
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})
let a;
readline.question('Enter Number : ', a => {
    if(a%2 == 0){
        console.log('Your number is Even');
    }else{
        console.log('Your number is Odd');
    }
readline.close();
})