import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChannelPageComponent } from './channel-page/channel-page.component';
import { HomeComponent } from './home/home.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { Task1Component } from './task1/task1.component';
import { Task2Component } from './task2/task2.component';
import { Task3Component } from './task3/task3.component';
import { Task4Component } from './task4/task4.component';
import { Task5Component } from './task5/task5.component';
import { Task6Component } from './task6/task6.component';

const routes: Routes = [
  {path:'Task-1', component:Task1Component},
  {path:'Task-2', component:Task2Component},
  {path:'Task-3', component:Task3Component},
  {path:'Task-4', component:Task4Component},
  {path:'Task-5', component:Task5Component},
  {path:'Task-6', component:Task6Component},
  {path:'home', component:HomeComponent},
  {path:'playlist', component:PlaylistComponent},
  {path:'channel-page', component:ChannelPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
