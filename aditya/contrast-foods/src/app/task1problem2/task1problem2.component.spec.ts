import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Task1problem2Component } from './task1problem2.component';

describe('Task1problem2Component', () => {
  let component: Task1problem2Component;
  let fixture: ComponentFixture<Task1problem2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Task1problem2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Task1problem2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
